#include <stdio.h>

void pattern(int a);
void row(int b);
int NumberOfRows=1;
int r;

void pattern(int a)
{
	if(a>0)
	{
		row(NumberOfRows);
		printf("\n");
		NumberOfRows++;
		pattern(a-1);

	}
}
void row(int b)
{
	if(b>0)
	{
		printf("%d",b);
		row(b-1);
	}
}

int main()
{
	printf("Enter the Number of Rows : ");
	scanf("%d",&r);
	pattern(r);
	return 0;
}
